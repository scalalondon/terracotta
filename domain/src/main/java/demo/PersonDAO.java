package demo;

import org.springframework.orm.hibernate3.HibernateTemplate;

public class PersonDAO extends HibernateTemplate implements IPersonDAO {
	
	@Override
	public Person findById(Long id) {
		return (Person) getSession().get(Person.class, id);
	}

	@Override
	public void saveOrUpdate(Person person) {
		super.saveOrUpdate(person);
	}

}
