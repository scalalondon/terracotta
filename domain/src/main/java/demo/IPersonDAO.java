package demo;

public interface IPersonDAO {

	Person findById(Long id);
	
	void saveOrUpdate(Person person);
	
}
